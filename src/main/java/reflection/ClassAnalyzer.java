package reflection;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

public class ClassAnalyzer {

  public static void parse(Class<?> clazz) throws Exception {
    System.out.println("Declared методы:");
    Method[] methods = clazz.getDeclaredMethods();
    methodsInfo(methods);

    System.out.println("\nМетоды без private:");
    methods = clazz.getMethods();
    methodsInfo(methods);
  }

  
  private static void methodsInfo(Method[] methods) {
    ArrayList<String> methodNames = new ArrayList<>();
    int pass = 0;
    int fail = 0;
    for (Method method : methods) {
      methodNames.add(method.getName());
      if (method.isAnnotationPresent(Secured.class)) {
        pass++;
      } else
        fail++;
    }

    System.out.println("Методов с аннотацией Secured:    " + pass + "\n"
        + "Методов без аннотации Secured:   " + fail + "\n" + "Методы: " + methodNames);

    Arrays.stream(methods).forEach((m) -> {
      System.out.print("    " + m.getName() + "  (");
      Arrays.stream(m.getParameters()).forEach((p) -> {
        System.out.print(p);
      });
      System.out.print(") ");
      Arrays.stream(m.getDeclaredAnnotations()).forEach((a)->System.out.print(a));
      
      
      System.out.print("\n"); //конец метода
    });

  }

}
