package reflection;

public class Entity {

  @SuppressWarnings("unused")
  @Secured(num = 1)
  public void Method1() {

    int i = 0;
  }

  @Secured(num = 2, name = "secondMethod")
  public Integer Method2(Integer j) {
    int i = j;
    return i;
  }
  
  @Secured(num=3)
  private String Method3(String value) {
    String stroka = "empty";
    return stroka;
  }

  @SuppressWarnings("unused")
  private String Method4(){
    return null;
  }
  
  @Deprecated
  public String Method5(){
    return null;
  }
}
